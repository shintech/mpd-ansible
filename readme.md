## getyouhits.com/assible-deploy

### Synopsis

Installs and deploys mpd

### Configuration

    # Edit the inventory file with the username, remote address and remote path

    [web:vars]
    ansible_ssh_user=ssh-user
    username=mpd
    music_directory=/mnt/Music
    remote_directory=/path/to/Music
    remote_address=192.168.0.1
    streaming_port=8080

### Usage

*Start script

    ansible-playlist -i inventory/web site.yml --ask-pass
